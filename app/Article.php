<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';

    protected $fillable = ['name', 'description', 'excerpt', 'published_at'];

    // scope
    public function scopePublished($query)
    {
        $query->where('published_at', '<=', Carbon::now());
    }

    /*public function scopeUnpublished($query)
    {

    }*/

    // mutator
    public function setPublishedAtAttribute($date)
    {
        //$this->attributes['published_at'] = date("m/d/Y", $date);

    }
}
