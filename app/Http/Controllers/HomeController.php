<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $first = "Niels";
        $last = "Vroman";

        return view('front/about')->with([
            'first' => $first,
            'last' => $last
        ]);
    }

    public function contact(){

        $people = ['Person A', 'Person B', 'Person C'];

        return view('front/contact', compact('people'));
    }
}
