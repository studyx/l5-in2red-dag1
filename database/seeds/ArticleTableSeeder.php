<?php

use Illuminate\Database\Seeder;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->insert([
            'name'          => 'Test Article',
            'description'   => str_random(40),
            'excerpt'       => str_random(20)
        ]);
    }
}
