@extends('app')

@section('content')
    <h2>{{ $article->name }}</h2>
    <p>{{ $article->description }}</p>

    <a style="float:left;" href="{{ action('ArticlesController@index') }}" class="btn btn-warning">Terug naar overzicht</a>

    <a style="float:left; margin-left:20px;" href="{{ action('ArticlesController@edit', [$article->id]) }}" class="btn btn-info">Editeren</a>

    {!! Form::open(array('route' => array('articles.destroy', $article->id), 'method' => 'DELETE')) !!}
    <button style="float:left; margin-left:20px;" class="btn btn-danger" type="submit">Verwijderen</button>
    {!! Form::close() !!}
@stop