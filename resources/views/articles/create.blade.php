@extends('app')

@section('content')
    <h1>Artikelen aanmaken!</h1>

    {!! Form::open(['action' => 'ArticlesController@store']) !!}

        @include('partials._form', ['buttonText' => 'Aanmaken'])

    {!! Form::close() !!}

@stop