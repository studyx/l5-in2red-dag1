@extends('app')

@section('content')
    @forelse($articles as $article)
    <article style="clear:both; margin-bottom: 80px;">
        <h2>{{ $article->name }}</h2>

        <p>
            {{ $article->excerpt }} <br/>
            <a href="{{ action('ArticlesController@show', [$article->id]) }}">Lees meer...</a>

        </p>

        <a style="float:left;" href="{{ action('ArticlesController@edit', [$article->id]) }}" class="btn btn-info">Editeren</a>

        {!! Form::open(array('route' => array('articles.destroy', $article->id), 'method' => 'DELETE')) !!}
            <button style="float:left; margin-left:20px;" class="btn btn-danger" type="submit">Verwijderen</button>
        {!! Form::close() !!}
    </article>

        <hr />

    @empty
        <p>Er zijn geen artikelen.</p>
    @endforelse
@stop