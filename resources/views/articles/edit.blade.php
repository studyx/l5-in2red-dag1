@extends('app')

@section('content')
    <h1>Artikel editeren!</h1>

    {!! Form::model($article, ['method' => 'PUT', 'url' => 'articles/' . $article->id]) !!}

        @include('partials._form', ['buttonText' => 'Updaten'])

    {!! Form::close() !!}

@stop