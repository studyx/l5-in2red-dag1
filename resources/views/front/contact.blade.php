@extends('app')

@section('content')

    <h2>Contacteer ons</h2>

    <p>Hier komt een formulier.</p>

    @if(count($people) > 0)
        @foreach($people as $person)
            {{ $person }}
        @endforeach
    @endif
@stop